\chapter{Preliminaries}

\section{Jets spaces}

Let $π \co M → X$ be a locally trivial fibration. A local section of $π$ is a
(smooth) map $s \co \Op \{m_0\} → X$ such that $π ∘ s = \Id$.

Let $s$ and $s'$ be two local section defined near $m_0$. 
We say that $s$ and $s'$ are tangent up to order $r$ at $m_0$ if
in a local trivialization where $π$ becomes $ℝ^n × ℝ^p → ℝ^n$, so that 
$s(m) = (m, f(m))$ and $s'(m) = (m, f'(m))$, $f$ and $f'$ have the same Taylor
expansion at $m_0$ up to order $r$.

\begin{enumerate}
  \item
    Prove that this condition is independent of the choice of local
    trivialization, hence well-defined.
\end{enumerate}

This condition defines an equivalence relation on the set of local sections near
$m_0$. The equivalence class of $s$ is denoted by $j^r s(m_0)$. The set of all
equivalence classes at all points $m_0$ is denoted by $X^{(r)}$, and its obvious
projection to $M$ is denoted by $π^r$. For any section $s \co U ⊂ M → X$, 
$m ↦ j^r s(m)$ is a section of $π^r$. We equip $X^{(r)}$ with the
quotient topology coming from the $C^r$ topology (any variant is fine
since we only care about neighborhoods of points). The next three
questions endow $X^{(r)}$ with a well defined and functorial smooth
structure.

\begin{enumerate}[resume]
  \item
  Prove that, for every (local) section $s$ defined near some $m_0$,
  there is a neighborhood $U$ of $m_0$ and a local homeomorphism 
  $Φ \co U × ℝ^N ↪ X^{(r)}$ such that $j^rs = Φ_0$ and each $Φ_p$ is a
  holonomic section.

  \item
    Prove that there is a unique smooth structure on $X^{(r)}$ such that $π^r$
  is smooth and every smooth family of sections $s_p$ (i.e. $s \co U ×
  ℝ^k → X$ is smooth and each $s_p$ is a section of $π$) 
  $(m, p) ↦ j^r s_p(m)$ is smooth.

  \item 
  For every local diffeomorphism $φ$ of $M$ and every $s$ we set
  $φ_*(j^r(m)) := j^r(s ∘ φ)(m)$. Prove that $φ_*$ is a local
  diffeomorphism of $X^{(r)}$.
\end{enumerate}

\section{Polyhedra and triangulations}
\label{sec:polyhedra_and_triangulations}

An affine cell $σ$ in $ℝ^N$ is the convex hull of a finite set of
points. Its dimension $\dim σ$ is the dimension of its affine span
$⟨σ⟩$.  Its interior $\Int σ$ is its topological interior inside $⟨σ⟩$.
Its boundary $∂σ$ is $σ ∖ \Int σ$. 

\begin{enumerate}
  \item
    Prove that $∂σ$ is a finite union of cells of dimension
    $\dim(σ) - 1$ (they are called faces of $σ$).
\end{enumerate}

A cell $σ$ which is the convex hull of exactly $\dim(σ) + 1$ points is
called a simplex. A cube is a cell which is an affine transform of 
$[0, 1]^d × \{0\}^{N-d}$.

A cell complex is a countable collection $K$ of cells $σ_i$ whose interiors
are pairwise disjoint, and whose faces are in $K$, and
such that every point of $ℝ^N$ is in finitely many cells. The union of
these cells, equipped with the induced topology, is denoted by $|K|$.

A cell complex $K'$ is a subdivision of $K$ if $|K'| = |K|$ and every
cell of $K'$ is contained in a cell of $K$. The $l$-skeleton of $K$ is
the collection of all cells in $K$ with dimension at most $l$.
The barycentric subdivision of a cell $σ$  is the subdivision obtained 
by adding as vertices the barycenters of $σ$ and all its faces, and then
adding all simplices of dimension at most $\dim σ$ determined by the old
and new vertices. The barycentric subdivision of a cell complex $K$ is
obtained by barycentric subdivisions of all its cells.

\begin{enumerate}[resume]
  \item
    Explain why the above definition of a barycentric subdivision of a
    cell complex makes sense.
  \item
    Prove that every cell complex has a subdivision whose cells are all
    simplices (resp. cubes).  
\end{enumerate}

A map $f$ from $|K|$ to a manifold $M$ is called piecewise smooth if,
for every cell $σ$ in $K$, there exists $f_σ \co \Op σ ⊂ ⟨σ⟩ → M$ which
is smooth and restricts to $f$ on $σ$.

A smooth polyhedron in a smooth manifold $M$ is $f(|K|)$ where $K$ is a
cell complex, and $f$ is a topological embedding that is piecewise
smooth. A cell decomposition of $M$ is $(K, f)$ where $f$ is a piecewise
smooth homeomorphism from $|K|$ to $M$. It is called a smooth
triangulation (resp. cubulation) if all cells are simplices (resp.
cubes).

Whitehead proved that every smooth manifold has a smooth triangulation
(hence also a cubulation). There is also a uniqueness result up to
suitable equivalence but stating it requires more definitions, and we
won't need it.

A manifold is called closed if it's compact and has empty boundary. An
open manifold is a manifold having no closed connected component.  The
goal of the next series of questions is to prove that, in every open
manifold $M$, there is a polyhedron $A$ with $\dim(A) < \dim(M)$ and,
for every neighborhood $U$ of $A$, an isotopy of embeddings 
$φ_t \co M ↪ M$ such that $φ_0 = \Id$, $φ_1(M) ⊂ U$ and $\rst{φ_t}{A} =
\Id_A$ for all $t$. 

The beginning happens purely on the combinatorial topology side. A graph
is a cell complex whose cells have dimension zero or one.
A graph isomorphic to $\{ [i, i + 1], i ∈ ℕ \}$ is called a ray. A
special tree is a graph which is the union of a ray and
finitely many disjoint tree having one vertex in the ray.

\begin{enumerate}[resume]

  \item
    Let $K$ be a triangulation of a connected non-compact $n$-manifold.
    Let $G$ be the one-skeleton of the dual triangulation, seen as a
    sub-complex of the (first) barycentric subdivision of $K$. Prove
    that $G$ contains a special tree $G'$ which contains all vertices of
    $G$.
  \item
    Let $L$ be the sub-complex of the
    $(n-1)$-skeleton of $K$ made of cells which do not intersect $G'$. 
    Let $Σ$ be a smooth hypersurface separating $M$ into a regular
    neighborhood $U$ of $L$ and a regular neighborhood $U'$ of $G'$
    (here regular means there is an isotopy of self-embeddings
    contracting the given neighborhood into an arbitrarily small one).
    Convince yourself that such an hypersurface $Σ$ exists and that
    $U'$ is diffeomorphic to $ℝ^{n-1} × ℝ^+$ (where $ℝ^+ = [0, +∞)$). 
    It may help to stare at the second barycentric subdivision of $K$.

  \item
    For every positive $ε$, consider $φ^ε_t \co ℝ^+ → ℝ^+$ defined by
    \[
      φ^ε_t(x) = \frac{x}{1 + t/ε x}.
    \]
    Prove that $t ↦ φ^ε_t$ is an isotopy of embeddings and $φ^ε_1$ 
    maps $ℝ^+$ to $[0, 1/ε)$.
  \item Conclude.
\end{enumerate}
