\chapter{Holonomic approximation}

The goal of this chapter is to prove Eliashberg and Mishachev's
holonomic approximation theorem:

\begin{theorem}
  Let $π \co X → V$ be a locally trivial fibration, and $A$ a polyhedron
  with positive codimension in $V$. 
  Let $\fF \co P × V → X^{(r)}$ be a smooth family of sections
  of $π^r$ parametrized by a compact manifold $P$, and such that
  $\fF_p$ is holonomic for $p$ in $\Op(∂P)$. For every positive
  functions $ε$ and $δ$ on $V$, there exists a family of isotopies
  $φ^p_t\co V → V$ and a family of holonomic sections $F$ such that:
  \begin{itemize}
    \item 
      $φ^p_t = \Id$ when $t = 0$ or $p ∈ \Op(P)$
    \item
      each $φ^p_t$ is $δ$-close to $\Id$ in $C^0$ topology
    \item
      each $F_p$ is defined on $\Op(φ^p_1(A))$
    \item
      $d\left(F_p(x), \fF_p(x)\right) ≤ ε(x)$ for all $x$ in $\Op(φ^p_1(A))$.    
  \end{itemize}  
\end{theorem}


\subsection*{Notations}

Ambient source space is $ℝ^n$, target space is $ℝ^p$. For
every point $x = (x_1, … , x_n)$ in $ℝ^n$ and every $l ≤ n$ we
set $x^l = (x_1, …, x_l)$. We set $I = [-1, 1]$ and, for each positive 
$s$, $I_s = [-1/s, 1/s]$. We always use the $\max$ distance on products:
$d(x, x') = \max d(x_i, x'_i)$. The $ε$-neighborhood of a subset 
$A ⊂ ℝ^n$ is $\N{ε}(A) := \{x ∈ ℝ^n \;;\; d(x, A) ≤ ε \}$.

We also fix an integer $k$ with $0 ≤ k < n$. Sections will be defined
near the cell $\C = I^k × \{0_{n-k}\} ⊂ ℝ^n$, where $0_{n-k}$ is the
origin of $ℝ^{n-k}$.

For any $l ≤ k$ and $y$ in $I^{k - l}$, we set
$\C_y = \{y\} × I^l × \{0_{n-k}\} ⊂ \C$.

Let $F = (F_y)$ be a smooth family of holonomic sections of
$J^r(ℝ^n, ℝ^p)$ defined on $\Op(\C_y)$, where $y$ is in $I^{k-l}$.
We say that $F$ is consistent near $∂\C$ if there is some holonomic
section $𝔽$ defined near $∂\C$ such that, for all $x$ in $\Op(∂C)$, 
$F_y(x) = 𝔽(x)$ whenever both are defined (in particular when $y$ is
in $∂I^{k-l}$ or $x$ is in $∂\C_y$).
%
The size $S(F)$ of such a family $F$ is the smallest integer such that:
\begin{itemize}
  \item
    each $F_y$ is defined on the $1/s$-neighborhood of $\C_y$~;
  \item
    $𝔽$ is defined on $\N{4/s}(∂\C)$~;
  \item
    the $C^1$ norm of $F$ is bounded by $s$ (including
    derivatives with respect to the parameter $y$).
\end{itemize}

We fix once and for all a cut-off function $χ \co ℝ^+ → [0, 1]$
such that $χ(d) = 0$ when $d ≤ 1/4$ and $χ(d) = 1$ when $d ≥ 3/4$.

Still keeping $n$, $k$ and $l$ fixed, for every pair of positive
integers $N$ and $S$, we set:
\[
  φ(x) = \left( x^{n-1},
  x_n + χ\big(Sd(x, ∂\C)\big)\frac{3}{4S}\cos(2πNSx_{k-l})\right).
\]
Note that this map commutes with the projection $x ↦ x^j$, for any 
$j < n$, and is the identity on $\N{1/4S}(∂C)$.

\begin{proposition}\label{prop:hol_construction}
  Let $F$ be a family of holonomic sections as above. For every positive
  integer $N$ and every integer $S ≥ S(F)$, there is a family $\bar F =
  (\bar F_z)$ where $z$ is in $I^{k-l-1}$, which is consistent near
  $∂C$ (with the same $𝔽$), each $\bar F_z$ is defined on
  $\Op(φ(\C_z))$ and, for all $x$ in $\Op(\C)$,
  \[
    d\left(F_{x^{k-l}}(φ(x)), \bar F_{x^{k-l-1}}(φ(x))\right) ≤
        \frac{B(S)}N
  \]
  for some function $B$ (recall that $x^j$ is the projection of $x$ onto
  $ℝ^j$).
\end{proposition}

\begin{enumerate}
  \item
    Explain why the inequality makes sense.
\end{enumerate}

\begin{proof}
  We fix a cutoff function $ρ \co ℝ → [0, 1]$ which equals $0$ before
  $-1/2$ and $1$ after $1/2$.
  Let $f$ be a family of sections representing $F$, where each $f_y$,
  $y ∈ I^{k-l}$, is defined on a neighborhood of $\N{1/S}(y)$.
  For each integer $i$ in $[0, 2NS]$, we set $t_i = -1 + i/(NS) ∈ I$.
  We first note that the mean value theorem gives
  \begin{multline}\label{eq:meanvalue}
    ∀ z ∈ I^{k-l-1}, ∀ t, t' ∈ I, ∀ x ∈ \N{1/S}(z, t) ∩ \N{1/S}(z, t'),\\
    d(F_{z, t}(x), F_{z, t'}(x)) ≤ S(F)|t - t'|.
  \end{multline}
  Then for each $i < 2NS$ and each $z$ in 
  $I^{k-l-1}$, we set
  \[
    f^i_z(x) = [1 - ρ(Sx_n)]f_{z, t_i}(x) + ρ(Sx_n)f_{z, t_{i+1}}(x)
  \]
  which is defined on a neighborhood of
  $(z + I_S^{k-l-1}) × [t_i, t_{i+1}]× I^l × I_S^{n-k}$.
  \begin{enumerate}[resume]
    \item
    Prove that
  \begin{equation*}
    \begin{aligned}
      j^rf^i_z(x) &= F_{z, t_i}(x) + 
      j^r\left[x ↦ ρ(Sx_n)\left(f_{z, t_{i+1}}(x)- f_{z, t_i}(x)\right) \right](x)\\
      &= F_{z, t_i}(x) + O\left(S^r\frac{S(F)}{NS}\right).
    \end{aligned}
  \end{equation*}
  \end{enumerate}

  Also note that $f^i_z(x) = f_{z, t_i}(x)$ when $x_n ≤ -1/(2S)$ and
  $f^i_z(x) = f_{z, t_{i+1}}(x)$ when $x_n ≥ 1/(2S)$. In particular
  $f^i_z(φ(x)) = f_{z, t_i}(φ(x))$ when $x$ is close to $(z, t_i, 0)$ while 
  $f^i_z(φ(x)) = f_{z, t_{i+1}}(φ(x))$ when $x$ is close to 
  $(z, (t_i + t_{i+1})/2, 0)$.

  So we define a family of sections near $φ(\C_z)$ by:
  \[
    \bar f_z(φ(x)) = 
    \begin{cases}
      f^i_z(φ(x)) & \text{if $x_{k-l} ∈ [t_i, (t_i + t_{i+1})/2]$} \\
      f_{z, t_{i+1}}(φ(x)) & \text{if $x_{k-l} ∈ [(t_i + t_{i+1})/2, t_{i+1}]$} 
    \end{cases}
  \]
  and then define $\bar F_z = j^r\bar f_z$.

  \begin{enumerate}[resume]
    \item
    Prove that $\bar F$ has the announced properties (don't forget to
    discuss what happens near $∂\C$).
  \end{enumerate}
\end{proof}

\begin{proposition}
Let $\fF$ be a smooth family of sections of $J^r(ℝ^n, ℝ^p)$ defined on
$\Op(\C)$.  Let $ε$ be a positive real number. For every sequence of
positive integers $N_l$, $1 ≤ l ≤ k$, there exists a sequence of
families $F^l$ of holonomic sections defines near sub-cells of dimension
$l$, and diffeomorphisms $φ_l$ preserving $x^k$ such that:
\begin{enumerate}
  \item
    $φ_0 = \Id$ and, for all $x$ in $\Op(\C)$,
    \begin{equation}
      d \left( F^0_{x^k}(x), \fF(x) \right) ≤ \frac{ε}{k+1}.
    \end{equation}

  \item
    Each pair $(F^l, φ_l)$ for $l ≥ 1$ depends on previous ones and
    $N_l$.
  \item
    for all $x$ in $\Op(\C_{x^{k-k-1}})$,
    \[
      d\left((φ_{l+1})_*F^{l+1}_{x^{k-l-1}}(φ_{l+1}(x)),\; F^l_{x^{k-l}}(φ_{l+1}(x))\right) ≤
      \frac{B(S(F^l))}{N_{l+1}}.
    \]
\end{enumerate}
\end{proposition}

\begin{enumerate}[resume]
  \item
    Prove the above proposition.
\end{enumerate}

We now prove the holonomic approximation lemma over a cube. For each
sequences $F^l$, $φ_l$ with parameters $N_l$ from the proposition, we
set $φ = φ_1 ∘ ⋯ ∘ φ_k$ and $F := φ_* F^k$. We also set, for $0 ≤ l ≤ k$,
$φ^l = φ_{l+1} ∘ ⋯ ∘ φ_k$, so that $φ^0 = φ$, $φ^l = φ_{l+1} ∘ φ^{l+1}$
and $φ^k = \Id$.

\begin{enumerate}
  \item 
    Prove that, for all $x$ in $\Op(\C)$ and $0 ≤ l < k$:
    \begin{multline*}
      d \left( φ^l_*F^k(φ^l(x)), F^l_{x^{k-l}}(φ^l(x)) \right) ≤\\ 
        K(φ_{l+1}) d\left( φ^{l+1}_*F^k(φ^{l+1}(x)),
       F^{l+1}_{x^{k-l-1}}(φ^{l+1}(x)) \right) +
      \frac{B(S(F^l))}{N_{l+1}}
    \end{multline*}
    for some nonnegative function $K$.
  \item
    Prove that, for all $x$ in $\Op(\C)$:
    \[
      d(F(φ(x)), \fF(φ(x))) ≤ \frac{ε}{k+1} + 
      \sum_{l=0}^{k-1} \prod_{j=1}^l K(φ_j)\frac{B(S(F^l))}{N_{l+1}}.
    \]
  \item
    Conclude the proof of the proposition.
\end{enumerate}

\begin{enumerate}[resume]
  \item
    Explain how to add a parameter $p ∈ I^d$ in the holonomic
    approximation theorem over a cube by reduction to the unparametric
    case in $ℝ^{n + d}$.
  \item
    Prove the theorem by induction over the cell decomposition of $A$ and some
		cell decomposition of $P$.
\end{enumerate}
