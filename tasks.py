from invoke import run, task

@task
def x(ctx):
    run('mkdir -p poly && cd src && xelatex -output-directory=../poly/ poly.tex')

@task
def poly(ctx):
    x(ctx)

@task
def web(ctx):
    run('cd src && plastex -c config web.tex')

@task 
def deploy(ctx):
    run('git checkout master')
    poly(ctx)
    web(ctx)
    target = 'sas1.math.u-psud.fr:wwwdoc/enseignement/h-principe/'
    run('scp poly/poly.pdf ' + target)
    run('scp -r web/* ' + target)
