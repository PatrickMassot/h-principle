# Build instructions

## Building the pdf version

This project requires compilation with XeLaTeX or LuaLaTeX, pdfLaTeX
will *not* work. It is recommended to use a separate directory for
output, eg. create `h-principle/poly` and then run `xelatex -output-directory=../poly/ poly.tex` from the `src` directory.

Alternatively, one can install [python invoke](http://www.pyinvoke.org/) and run `inv poly`.

## Building the web version

### First time setup

System-wide part (requires sudo):

* install python3 and pip
* pip install virtualenvwrapper

Setting up a python virtualenv:

* mkvirtualenv plastex --python=/usr/bin/python3
* pip install jinja2 bs4 unidecode nodeenv invoke

Installing plastex:
* git clone -b h-principle https://github.com/PatrickMassot/plastex.git
* cd plastex
* pip install .

Setting up a node virtualenv inside the python virtual env, and install
mathjax-node-page

* nodeenv -p
* npm install -g mathjax-node-page

### Building

* workon plastex
* cd h-principle
* inv web
